# ruby-test

Suppose that we have a table, sensor_updates, to keep track of sensor updates with the following fields:

```
sensor_id: integer -- references the unique ID of sensors
time: timestamp
state: string -- sample values: "good", "bad", ...
```

A sensor is supposed to be in a state from the time indicated until next update comes in. If there were no updates arriving for more than 10 seconds after an update, the sensor will be considered to be in `DOWN` state after the 10 second period until another update arrives. For clarification, there will be now updates arriving with DOWN state and the `DOWN` state is calculated by the API when there are no updates coming in for more 10 seconds.

You are asked to write an Rails API endpoint that implements the following requirements in an efficient way:

Endpoint takes the following parameters: start_time (timestamp) and duration (integer value for the number of seconds).
For each distinct sensor_id that appears in this table, endpoint returns an array listing the durations and percentages that the sensors was in for each distinct state. Example output:

```JSON
"1": { // sensor_id

    "good": {

            duration: 900,  // total number of seconds that sensor 1 spent in "good" state

            percentage: 90

    },

    "bad": {

            duration: 50,

            percentage: 5

    },

    "DOWN": {

            duration: 50,

            percentage: 5

    }

},

"2": {

    ...

}

...
```
